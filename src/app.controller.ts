import {
  Controller,
  Get,
  Param,
  Body,
  Req,
  // Post,
  // Body,
  // Put,
  Patch,
  // Delete,
} from '@nestjs/common';
import { UsersService } from './users/users.service';
import { ListsService } from './lists/lists.service';
import { User as UserModel, List as ListModel } from '@prisma/client';
import { Prisma } from '@prisma/client';

@Controller()
export class AppController {
  constructor(
    private readonly usersService: UsersService,
    private readonly listsService: ListsService,
  ) {}

  @Get('users')
  async getUsers(): Promise<UserModel[]> {
    return this.usersService.users({});
  }

  @Get('user/:id')
  async getUserById(@Param('id') id: string): Promise<UserModel> {
    return this.usersService.user({ id: Number(id) });
  }

  @Get('user/:userId/lists')
  async getListsByUser(@Param('userId') userId: string): Promise<ListModel[]> {
    return this.usersService.listsByUser({ id: Number(userId) });
  }

  @Get('/list/:id')
  async getListById(@Param('id') id: string): Promise<ListModel> {
    return this.listsService.list({ id: Number(id) });
  }

  @Patch('/list/:id')
  async updateList(
    @Param('id') id: string,
    @Body() data: Prisma.ListUpdateInput,
  ): Promise<ListModel> {
    return this.listsService.updateList({
      where: {
        id: Number(id),
      },
      data: data,
    });
  }
}
