/*
  Warnings:

  - A unique constraint covering the columns `[listId,name]` on the table `Item` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[authorId,title]` on the table `List` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Item_listId_name_key" ON "Item"("listId", "name");

-- CreateIndex
CREATE UNIQUE INDEX "List_authorId_title_key" ON "List"("authorId", "title");
