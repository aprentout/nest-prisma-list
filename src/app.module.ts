import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { PrismaService } from './app.service';
import { ListsService } from './lists/lists.service';
import { UsersService } from './users/users.service';
import { ItemsService } from './items/items.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [PrismaService, ListsService, UsersService, ItemsService],
})
export class AppModule {}
