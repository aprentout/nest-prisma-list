import { Injectable } from '@nestjs/common';
import { PrismaService } from '../app.service';
import { List, Prisma } from '@prisma/client';

@Injectable()
export class ListsService {
  constructor(private prisma: PrismaService) {}

  async list(
    listWhereUniqueInput: Prisma.ListWhereUniqueInput,
  ): Promise<List | null> {
    return this.prisma.list.findUnique({
      where: listWhereUniqueInput,
    });
  }

  async lists(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.ListWhereUniqueInput;
    where?: Prisma.ListWhereInput;
    orderBy?: Prisma.ListOrderByWithRelationInput;
  }): Promise<List[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.list.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async createList(data: Prisma.ListCreateInput): Promise<List> {
    return this.prisma.list.create({
      data,
    });
  }

  async updateList(params: {
    where: Prisma.ListWhereUniqueInput;
    data: Prisma.ListUpdateInput;
  }): Promise<List> {
    const { data, where } = params;
    return this.prisma.list.update({
      data,
      where,
    });
  }

  async deleteList(where: Prisma.ListWhereUniqueInput): Promise<List> {
    return this.prisma.list.delete({
      where,
    });
  }
}
