import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();
async function main() {
  const antoine = await prisma.user.upsert({
    where: { email: 'antoine@test.com' },
    update: {},
    create: {
      email: 'antoine@test.com',
      name: 'Antoine',
      lists: {
        create: [
          {
            title: 'weekly courses',
            status: false,
            items: {
              create: [
                {
                  name: 'tomato',
                  quantity: 2,
                  status: false,
                },
                {
                  name: 'banana',
                  quantity: 5,
                  status: true,
                },
              ],
            },
          },
        ],
      },
    },
  });
  const timmy = await prisma.user.upsert({
    where: { email: 'timmy@test.com' },
    update: {},
    create: {
      email: 'timmy@test.com',
      name: 'Timmy',
      lists: {
        create: [
          {
            title: 'stuff',
            status: true,
            items: {
              create: [
                {
                  name: 'mouse',
                  quantity: 1,
                  status: true,
                },
                {
                  name: 'keyboard',
                  quantity: 1,
                  status: true,
                },
              ],
            },
          },
        ],
      },
    },
  });
  console.log({ antoine, timmy });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
